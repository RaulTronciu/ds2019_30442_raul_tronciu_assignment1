﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Dtos;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineMedicationApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class DoctorController : Controller
    {
        private readonly IDoctorService _doctorService;
        private readonly SoapService.SoapSoapClient _soapService = new SoapService.SoapSoapClient(new BasicHttpBinding(), new EndpointAddress("http://localhost:61670/Soap.asmx"));

        public DoctorController(IDoctorService doctorService)
        {
            _doctorService = doctorService;
        }

        [HttpGet("getPatients")]
        public async Task<ActionResult> GetPatients()
        {
            try
            {
                return await _doctorService.GetPatientsAsync();
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("getCaregivers")]
        public async Task<ActionResult> GetCaregivers()
        {
            try
            {
                return await _doctorService.GetCaregiversAsync();
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("updatePatient")]
        public async Task<ActionResult> UpdatePatient([FromBody]UpdateEntityDto user)
        {
            try
            {
                return await _doctorService.UpdatePatientAsync(user);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("updateUser")]
        public async Task<ActionResult> UpdateUser([FromBody]CaregiverForTable user)
        {
            try
            {
                return await _doctorService.UpdateUserAsync(user);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("deleteUser")]
        public async Task<ActionResult> DeleteUser(string phone)
        {
            try
            {
                return await _doctorService.DeleteUserAsync(phone);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("patientHistory")]
        public ActionResult GetPatientHistory(string phone)
        {
            try
            {
                var list = _soapService.GetPatientHistoryAsync(phone).Result.Body.GetPatientHistoryResult.ToList().Select(x =>
                {
                    return new ActivityDto
                    {
                        Id = x.Id,
                        UserId = x.UserId,
                        ActivityName = x.ActivityName,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        NormalActivity = x.NormalActivity
                    };
                }).ToList();
                return Ok(list);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpGet("patientMedicalHistory")]
        public ActionResult GetPatientMedicalHistory(string phone)
        {
            try
            {
                var list = _soapService.GetPatientMedicalHistoryAsync(phone).Result.Body.GetPatientMedicalHistoryResult.ToList().Select(x =>
                {
                    return new MedicalActivitiesDto
                    {
                        Id = x.Id,
                        UserId = x.UserId,
                        Date = x.Date,
                        CorrectlyTaken = x.CorrectlyTaken
                    };
                }).ToList();
                return Ok(list);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("addRecommandation")]
        public ActionResult AddRecommandation([FromBody]RecommandationDto recommandationDto)
        {
            try
            {
                var result = _soapService.AddRecommandationAsync(recommandationDto.Phone, recommandationDto.Recommandation).Result.Body.AddRecommandationResult;
                return Ok(result);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("addCondition")]
        public ActionResult AddCondition([FromBody]ConditionDto conditionDto)
        {
            try
            {
                var result = _soapService.AddConditionAsync(conditionDto.Condition, conditionDto.Id).Result;
                return Ok(result);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
