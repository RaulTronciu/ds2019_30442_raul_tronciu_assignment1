﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineMedicationApp.BusinessLogic.Abstract;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineMedicationApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class PatientController : Controller
    {
        private readonly IPatientService _patientService;

        public PatientController(IPatientService patientService)
        {
            _patientService = patientService;
        }

        [HttpGet("getPatientDetails")]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                return await _patientService.GetDetailsAsync(id);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
