﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Dtos;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OnlineMedicationApp.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("login")]
        public async Task<ActionResult> Post([FromBody] LoginCredentialsDto loginCredentialsDto)
        {
            try
            {
                var result = await _userService.LoginAsync(loginCredentialsDto);

                return Ok(result);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost("register")]
        public async Task<ActionResult> Post([FromBody] UserDto userDto)
        {
            try
            {
                var result = await _userService.RegisterAsync(userDto);

                return Ok(result);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
