﻿using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Dtos;
using OnlineMedicationApp.BusinessObjects.Models;
using OnlineMedicationApp.BusinessObjects.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Concrete
{
    public class UserService : IUserService
    {
        private readonly OnlineMedicationContext _dbContext;
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IMapper _mapper;

        public UserService(OnlineMedicationContext dbContext, ITokenGenerator tokenGenerator, IMapper mapper)
        {
            _dbContext = dbContext;
            _tokenGenerator = tokenGenerator;
            _mapper = mapper;
        }

        public async Task<ActionResult> LoginAsync(LoginCredentialsDto loginCredentialsDto)
        {
            var result = new JsonResult(StringResources.WRONG_CREDENTIALS)
            {
                StatusCode = 401
            };

            var userCredentials = await _dbContext.Set<Credentials>()
                .Where(credentials => credentials.Email == loginCredentialsDto.Email && credentials.Password == loginCredentialsDto.Password)
                .FirstOrDefaultAsync();

            if (userCredentials != null)
            {
                var user = await _dbContext.Set<User>()
                    .Where(l => l.Id == userCredentials.Id)
                    .FirstOrDefaultAsync();

                userCredentials.User = user;

                var token = _tokenGenerator.GenerateToken(userCredentials);

                result = new JsonResult(token)
                {
                    StatusCode = 200
                };
            }

            return result;
        }

        public async Task<ActionResult> RegisterAsync(UserDto userDto)
        {
            var user = _mapper.MapUserDtoToUser(userDto);

            var possibleUserInDb = await _dbContext.Set<User>()
                .Where(u => u.Credentials.Email == user.Credentials.Email || u.Credentials.Phone == user.Credentials.Phone)
                .FirstOrDefaultAsync();

            if (possibleUserInDb == null)
            {
                await _dbContext.Set<User>().AddAsync(user);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                return new JsonResult(StringResources.ALREADY_EXISTS)
                {
                    StatusCode = 400
                };
            }

            return new JsonResult(StringResources.SUCCESS_REGISTER)
            {
                StatusCode = 200
            };
        }
    }
}
