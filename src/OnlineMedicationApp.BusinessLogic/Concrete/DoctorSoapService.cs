﻿using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineMedicationApp.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Concrete
{
    public class DoctorSoapService
    {
        private readonly OnlineMedicationContext _dbContext;

        public DoctorSoapService(OnlineMedicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Activities> GetPatientHistory(string phone)
        {
            var credentials = _dbContext.Set<Credentials>()
                .Where(c => c.Phone == phone)
                .FirstOrDefault();

            var activitiesHistory = _dbContext.Set<Activities>()
                .Where(ac => ac.Id == credentials.Id)
                .ToList();

            return activitiesHistory;
        }

        public List<MedicationActivity> MedicationPerDay(string phone)
        {
            var credentials = _dbContext.Set<Credentials>()
                .Where(c => c.Phone == phone)
                .FirstOrDefault();

            var medicationActivity = _dbContext.Set<MedicationActivity>()
                .Where(ma => ma.Id == credentials.Id)
                .ToList();

            return medicationActivity;
        }

        public bool AddCondition(bool condition, int id)
        {
            var activity = _dbContext.Set<Activities>()
               .Where(ac => ac.Id == id)
               .FirstOrDefault();

            activity.NormalActivity = condition;

            _dbContext.SaveChanges();

            return true;
        }

        public bool AddRecommandation(string phone, string recommandation)
        {
            var credentials = _dbContext.Set<Credentials>()
                .Where(c => c.Phone == phone)
                .FirstOrDefault();

            var user = _dbContext.Set<User>()
                .Where(u => u.Id == credentials.Id)
                .FirstOrDefault();

            user.Recommandation = recommandation;

            _dbContext.SaveChanges();

            return true;
        }
    }
}
