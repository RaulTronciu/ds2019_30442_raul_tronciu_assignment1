﻿using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Models;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Concrete
{
    public class PatientService : IPatientService
    {
        private readonly OnlineMedicationContext _dbContext;

        public PatientService(OnlineMedicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ActionResult> GetDetailsAsync(int id)
        {
            var result = new JsonResult("Hah")
            {
                StatusCode = 404
            };

            var user = await _dbContext.Set<User>()
                .Where(l => l.Id == id)
                .FirstOrDefaultAsync();

            if (user != null)
            {
                result = new JsonResult(user)
                {
                    StatusCode = 200
                };
            }

            return result;
        }
    }
}
