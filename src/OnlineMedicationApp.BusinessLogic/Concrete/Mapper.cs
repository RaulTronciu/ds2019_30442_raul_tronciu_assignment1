﻿using OnlineMedicationApp.BusinessLogic.Abstract;
using OnlineMedicationApp.BusinessObjects.Dtos;
using OnlineMedicationApp.BusinessObjects.Models;
using System.Collections.Generic;

namespace OnlineMedicationApp.BusinessLogic.Concrete
{
    public class Mapper : IMapper
    {
        public User MapUserDtoToUser(UserDto userDto)
        {
            return new User()
            {
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Credentials = MapCredentialsDtoToCredentials(userDto.Credentials),
                MedicationPlan = new MedicationPlan(),
                Activities = new List<Activities>(),
                MedicationActivities = new List<MedicationActivity>()
            };
        }

        private Credentials MapCredentialsDtoToCredentials(CredentialsDto credentialsDto)
        {
            return new Credentials()
            {
                Email = credentialsDto.Email,
                Password = credentialsDto.Password,
                Phone = credentialsDto.Phone
            };
        }
    }
}
