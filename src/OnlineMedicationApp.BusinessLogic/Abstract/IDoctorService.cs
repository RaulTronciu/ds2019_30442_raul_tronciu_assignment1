﻿using Microsoft.AspNetCore.Mvc;
using OnlineMedicationApp.BusinessObjects.Dtos;
using OnlineMedicationApp.BusinessObjects.Enums;
using System.Threading.Tasks;

namespace OnlineMedicationApp.BusinessLogic.Abstract
{
    public interface IDoctorService
    {
        Task<ActionResult> GetPatientsAsync();
        Task<ActionResult> GetCaregiversAsync();
        Task<ActionResult> UpdatePatientAsync(UpdateEntityDto user);
        Task<ActionResult> UpdateUserAsync(CaregiverForTable user);
        Task<ActionResult> DeleteUserAsync(string phone);
    }
}
