﻿using OnlineMedicationApp.BusinessObjects.Dtos;
using OnlineMedicationApp.BusinessObjects.Models;

namespace OnlineMedicationApp.BusinessLogic.Abstract
{
    public interface IMapper
    {
        User MapUserDtoToUser(UserDto userDto);
    }
}
