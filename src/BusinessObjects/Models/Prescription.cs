﻿namespace OnlineMedicationApp.BusinessObjects.Models
{
    public class Prescription
    {
        public int Id { get; set; }
        public string MedicationName { get; set; }
        public string IntakeInterval { get; set; }
        public string Observation { get; set; }
        public MedicationPlan MedicationPlan { get; set; }
    }
}
