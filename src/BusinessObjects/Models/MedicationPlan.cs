﻿using System.Collections.Generic;

namespace OnlineMedicationApp.BusinessObjects.Models
{
    public class MedicationPlan
    {
        public int Id { get; set; }
        public string TreatmentPeriod { get; set; }
        public IList<Prescription> Prescription { get; set; }
        public User User { get; set; }
    }
}
