﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMedicationApp.BusinessObjects.Utils
{
    public class StringResources
    {
        public const string ALREADY_EXISTS = "Email or Phone already used.";
        public const string WRONG_REGISTER_INFORMATION = "Something went wrong. Please try again.";
        public const string SUCCESS_REGISTER = "Registered with success.";
        public const string WRONG_CREDENTIALS = "Wrong Credentials combination.";
    }
}
