﻿using OnlineMedicationApp.BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMedicationApp.BusinessObjects.Dtos
{
    public class MedicationPlanDto
    {
        public string TreatmentPeriod { get; set; }
        public List<Prescription> Prescription { get; set; }
    }
}
