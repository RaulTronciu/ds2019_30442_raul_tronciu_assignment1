﻿namespace OnlineMedicationApp.BusinessObjects.Dtos
{
    public class UserDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public CredentialsDto Credentials { get; set; }
    }
}
