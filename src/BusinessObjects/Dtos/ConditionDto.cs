﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMedicationApp.BusinessObjects.Dtos
{
    public class ConditionDto
    {
        public bool Condition { get; set; }
        public int Id { get; set; }
    }
}
