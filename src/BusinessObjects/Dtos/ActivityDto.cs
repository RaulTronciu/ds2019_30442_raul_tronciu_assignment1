﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineMedicationApp.BusinessObjects.Dtos
{
    public class ActivityDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string ActivityName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public bool NormalActivity { get; set; }
    }
}
